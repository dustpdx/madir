# Mutal Aid Directory

Howdy! Glad youve found me! This is intended to be an open source project to compile resources, where theyre at in the world, and how to contact them so all of us can be better connected, so feel free to make edits, add notes about things, share this around, etc. 

Some things to note before you do though
1. Only keep text files here
2. If you know the native names of places, please add it alongside the colonial names
2. Please keep things as tidy as you can. We want to keep this as easy as possible to use and navigate, so please dont make a mess.

Thank you for reading this, stay safe and stay dangerous
